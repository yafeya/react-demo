import { RouteComponentProps, Link } from "react-router-dom";
import logo from '../logo.svg';
import { Image } from "../Image/Image";
import { LikeWrapper } from "../Like/Like";
import { ClockWrapper } from "../Clock/Clock";

interface ReduxProps extends RouteComponentProps {
}

export const Redux: React.FC<ReduxProps> = (props) => {
    return (
        <div className="App">
            <header className="App-header">
                <Image src={logo} alt="logo"></Image>
                <LikeWrapper></LikeWrapper>
                <ClockWrapper></ClockWrapper>
                <Link to="/">Home</Link>
                <button className="btn btn-primary" onClick={props.history.goBack}>Back</button>
            </header>
        </div>
    );
}