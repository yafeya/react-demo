import { Dispatch } from "react";
import { Component } from "react";
import { connect } from "react-redux";
import { AnyAction } from "redux";
import { onLike } from "./LikeRedux";
import { State } from "../models/State";

export interface LikeProps {
    count: number;
    onLike(count: number): void;
}

export class Like extends Component<LikeProps> {
    constructor(props: LikeProps) {
        super(props);
        this.state = {
            count: props.count
        };
    }

    onClick() {
        let count = this.props.count + 1;
        this.props.onLike(count);
    }

    render() {
        return (
            <div>
                <p>There are {this.props.count} person like you!!</p>
                <button className="btn btn-primary" onClick={() => this.onClick()}>Like</button>
            </div>
        );
    }
}

const mapStateToProps = (state: State) => {
    const { count } = state.like;
    return { count };
};

const mapDispatchToProps = (dispatch: Dispatch<AnyAction>) => ({
    onLike: (count: number) => dispatch(onLike(count))
});

export const LikeWrapper = connect(mapStateToProps, mapDispatchToProps)(Like);