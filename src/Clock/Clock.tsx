import React, { useEffect, useRef } from "react";
import { connect } from "react-redux";
import { AnyAction } from "redux";
import { ThunkDispatch } from "redux-thunk";
import { fetchTime } from "./ClockRedux";
import { State } from "../models/State";

interface ClockProps {
    time: Date;
    fetchTime: (parameter: string) => void;
}

export const Clock: React.FC<ClockProps> = (props) => {
    const clockRef = useRef<HTMLParagraphElement>(null);

    useEffect(() => {
        setInterval(() => props.fetchTime('test-parameter'), 1000);
    }, [clockRef]);

    return <p ref={clockRef}>The current time is {props.time.toLocaleTimeString()}</p>;
};

const mapStateToProps = (state: State) => {
    const { time } = state.clock;
    return { time };
}

const mapDispatchToProps = (dispatch: ThunkDispatch<any, any, AnyAction>) => ({
    fetchTime: (parameter: string) => dispatch(fetchTime(parameter))
});

export const ClockWrapper = connect(mapStateToProps, mapDispatchToProps)(Clock);