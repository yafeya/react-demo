import { Component } from "react";

export interface ParagraphProps {
    part1: string;
    part2: string;
    code: string;
}

export class Paragraph extends Component<ParagraphProps, {}>{
    constructor(props: ParagraphProps){
        super(props);
    }

    render(){
        return (
            <p>
                {this.props.part1} <code>{this.props.code}</code> {this.props.part2}
            </p>
        )
    }
}