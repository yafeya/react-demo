FROM nginx:stable-alpine

RUN rm -fr /usr/react-demo
RUN mkdir -p /usr/react-demo
COPY ./dist/ /usr/react-demo/

COPY ./nginx.conf /etc/nginx/conf.d/default.conf

RUN ls /usr/react-demo

EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]