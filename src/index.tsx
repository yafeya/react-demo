import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import { App } from './App/App';
import reportWebVitals from './reportWebVitals';
import 'bootstrap/dist/css/bootstrap.css';
import { Provider } from 'react-redux';
import { clockReducer } from './Clock/ClockRedux';
import { likeReducer } from './Like/LikeRedux';
import { combineReducers, createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { itemsReducer } from './ItemList/ItemRedux';

const appReducer = combineReducers({
  clock: clockReducer,
  like: likeReducer,
  items: itemsReducer
});
const middleware = [thunk];
const store = createStore(appReducer, applyMiddleware(...middleware));

ReactDOM.render(
  <Provider store={store}>
    <React.StrictMode>
      <App />
    </React.StrictMode>
  </Provider>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
