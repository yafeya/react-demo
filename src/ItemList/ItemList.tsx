import { RouteComponentProps, Link } from "react-router-dom";
import logo from '../logo.svg';
import { Image } from "../Image/Image";
import { State } from "../models/State";
import { ThunkDispatch } from "redux-thunk";
import { AnyAction } from "redux";
import { fetchItems } from "./ItemRedux";
import { connect } from "react-redux";
import { Item } from "../models/item";
import './itemList.scss';
import { useEffect } from "react";

interface ItemsProps extends RouteComponentProps {
    items: Item[],
    fetchItems(): void;
}

export const Items: React.FC<ItemsProps> = (props) => {

    useEffect(() => {
        props.fetchItems();
    });

    return (
        <div className="App">
            <header className="App-header">
                <Image src={logo} alt="logo"></Image>
                <div className="itemsWrapper">
                    <div className="itemHeaderWrapper">
                        <span className="itemTitle">Name</span>
                        <span className="operationWrapper">Operation</span>
                    </div>
                    {
                        props.items.map((item, index) =>
                            <div key={item.id} className="itemWrapper" data-index={index % 2}>
                                <span className="itemTitle">{item.name}</span>
                                <Link className="operationWrapper btn btn-primary" to={`/item/${item.id}`}>Detail</Link>
                            </div>
                        )
                    }
                </div>
                <Link to="/">Home</Link>
                <button className="btn btn-primary" onClick={props.history.goBack}>Back</button>
            </header>
        </div>
    );
}

const mapStateToProps = (state: State) => {
    const { items } = state.items;
    return { items };
}

const mapDispatchToProps = (dispatch: ThunkDispatch<any, any, AnyAction>) => ({
    fetchItems: () => dispatch(fetchItems())
});

const ItemsWrapper = connect(mapStateToProps, mapDispatchToProps)(Items);

export default ItemsWrapper;