import { Action, ActionCreator, AnyAction } from "redux";
import { ThunkAction, ThunkDispatch } from "redux-thunk";

export interface ClockState {
    time: Date;
    isFetching: boolean;
    succeed: boolean;
}

const FETCHING_TIME = 'FETCHING_TIME';
const FETCHED_TIME = 'FETCHED_TIME';

export interface FetchingTimeAction {
    type: string
};

export interface FetchedTimeAction {
    type: string;
    payload: Date;
}

// Mock Api
function getTimeAsync(): Promise<Date> {
    return new Promise<Date>((resolve, reject) => {
        resolve(new Date());
    });
}

//Action Creator
export function fetchTime(parameter: string): ThunkAction<
                                                        // Promise of last dispatched action
                                                        Promise<FetchedTimeAction>,
                                                        // Data type of the last action
                                                        Date,
                                                        // The type of the parameter for the nested function
                                                        string,
                                                        // The type of last action to dispatch.
                                                        FetchedTimeAction> {
            return async (dispatch: ThunkDispatch<Date, string, AnyAction>)=>{
                console.log(`parameter is ${parameter}`);
                let fetchingAction:FetchingTimeAction = {type: FETCHING_TIME};
                // we can dispatch middle action in the creator.
                dispatch(fetchingAction);
                let time = await getTimeAsync();
                let fetchedTimeAction: FetchedTimeAction = {
                    type: FETCHED_TIME,
                    // property name must be 'payload'
                    payload: time
                };
                return dispatch(fetchedTimeAction);
            };
        };

const initState: ClockState = {
    time: new Date(),
    isFetching: false,
    succeed: true
};

export function clockReducer(state = initState, action: any): ClockState {
    let result = initState;
    switch (action.type) {
        case FETCHING_TIME:
            result = {
                ...state,
                isFetching: true
            };
            break;
        case FETCHED_TIME:
            result = {
                ...state,
                isFetching: false,
                succeed: true,
                time: action.payload
            };
            break;
        default:
            result = state;
            break;
    }
    return result;
}