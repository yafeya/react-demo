import React from 'react';
import './Image.scss';

export interface ImageProps{
    src: string;
    alt: string;
}

export const Image: React.FC<ImageProps> = (props)=>{
    return (<img src={props.src} className="App-logo" alt={props.alt} />);
};