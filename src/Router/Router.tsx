import { BrowserRouter, Redirect, Route, Switch } from "react-router-dom";
import { Home } from "../Home/Home";
import { Redux } from "../Redux/Redux";
import { ItemDetailWrapper } from "../ItemDetail/Item";
import { lazy, Suspense } from "react";


const TranslationWrapper = lazy(() => import("../i18n/translation"));
const ItemsWrapper = lazy(() => import("../ItemList/ItemList"));

export const Router = () => {
    let subdir = process.env.REACT_APP_SUB_DIR;
    return (
        <BrowserRouter basename={subdir}>
            <Suspense fallback={<div>loading...</div>}>
                <Switch>
                    <Route exact={true} path="/" component={Home} />
                    <Route path="/redux" component={Redux} />
                    <Route path="/items" component={ItemsWrapper} />
                    <Route path="/item/:id" component={ItemDetailWrapper} />
                    <Route path="/i18n" component={TranslationWrapper} />
                    <Route component={() => <Redirect to="/" />} />
                </Switch>
            </Suspense>
        </BrowserRouter> 
    );
}