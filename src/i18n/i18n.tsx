import i18n from "i18next";
import LanguageDetector from 'i18next-browser-languagedetector';
import enResources from "./en.json";
import zhResources from "./zh.json";
import { initReactI18next } from 'react-i18next';

const DETECTION_OPTIONS = {
  order: ['path', 'querystring']
}

i18n.use(LanguageDetector) //获取当前浏览器语言
.use(initReactI18next) 
.init({
  detection: DETECTION_OPTIONS,
  //资源文件
  resources: {
    en: {
      translation: enResources,
    },
    zh: {
      translation: zhResources,
    },
  },
  fallbackLng: "zh",
  debug: false,
  interpolation: {
    escapeValue: false,
  },
})
 
export default i18n;