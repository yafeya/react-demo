import { AnyAction } from "redux";
import { ThunkAction, ThunkDispatch } from "redux-thunk";
import { Item } from "../models/item";

export interface ItemListState {
    items: Item[];
    isFetching: boolean;
    succeed: boolean;
}

const FETCHING_ITEMS = 'FETCHING_ITEMS';
const FETCHED_ITEMS = 'FETCHED_ITEMS';

export interface FetchingItemsAction {
    type: string
};

export interface FetchedItemsAction {
    type: string;
    payload: Item[];
}

// Mock Api
function getItemsAsync(): Promise<Item[]> {
    return new Promise<Item[]>((resolve, reject) => {
        resolve([
            { id: 'item1', name: 'item1', description: 'this is item1' },
            { id: 'item2', name: 'item2', description: 'this is item2' },
            { id: 'item3', name: 'item3', description: 'this is item3' },
            { id: 'item4', name: 'item4', description: 'this is item4' },
            { id: 'item5', name: 'item5', description: 'this is item5' },
        ]);
    });
}

//Action Creator
export function fetchItems(): ThunkAction<
    // Promise of last dispatched action
    Promise<FetchedItemsAction>,
    // Data type of the last action
    Item[],
    // The type of the parameter for the nested function
    unknown,
    // The type of last action to dispatch.
    FetchedItemsAction> {
    return async (dispatch: ThunkDispatch<Date, unknown, AnyAction>) => {
        let fetchingAction: FetchingItemsAction = { type: FETCHING_ITEMS };
        // we can dispatch middle action in the creator.
        dispatch(fetchingAction);
        let items = await getItemsAsync();
        let fetchedItemsAction: FetchedItemsAction = {
            type: FETCHED_ITEMS,
            // property name must be 'payload'
            payload: items
        };
        return dispatch(fetchedItemsAction);
    };
};

const initState: ItemListState = {
    items: [],
    isFetching: false,
    succeed: true
};


export function itemsReducer(state = initState, action: any): ItemListState {
    let result = initState;
    switch (action.type) {
        case FETCHING_ITEMS:
            result = {
                ...state,
                isFetching: true
            };
            break;
        case FETCHED_ITEMS:
            result = {
                ...state,
                isFetching: false,
                succeed: true,
                items: action.payload
            };
            break;
        default:
            result = state;
            break;
    }
    return result;
}
