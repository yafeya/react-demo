import React, { Component } from 'react';
import './App.scss';
import { Router } from '../Router/Router';
import '../i18n/i18n';

export class App extends Component {

  render() {
    console.log(`Environment: ${process.env.NODE_ENV}`);
    console.log(`Base Url: ${process.env.REACT_APP_API_BASEURL}`);
    console.log(`PUBLIC_URL: ${process.env.PUBLIC_URL}`);
    console.log(`Sub-directory: ${process.env.REACT_APP_SUB_DIR}`);

    return (
      <Router/>
    );
  }
}