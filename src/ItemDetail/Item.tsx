import { RouteComponentProps, Link } from "react-router-dom";
import logo from '../logo.svg';
import { Image } from "../Image/Image";
import { State } from "../models/State";
import { connect } from "react-redux";
import { Item } from "../models/item";
import QueryString from "querystring";
import { useEffect, useState } from "react";

interface RouteParams {
    id: string
}

interface ItemProps extends RouteComponentProps<RouteParams> {
    items: Item[];
}

export const ItemDetail: React.FC<ItemProps> = (props) => {

    const id = props.match.params.id;
    const [item, setItem] = useState(props.items.find(x => x.id == id));

    return (
        <div className="App">
            <header className="App-header">
                <Image src={logo} alt="logo"></Image>
                <div className="itemDetailWrapper">
                    <div>ID:</div>
                    <div>{item?.id}</div>
                    <div>Name:</div>
                    <div>{item?.name}</div>
                    <div>Description:</div>
                    <div>{item?.description}</div>
                </div>
                <Link to="/">Home</Link>
                <button className="btn btn-primary" onClick={props.history.goBack}>Back</button>
            </header>
        </div>
    );
}

const mapStateToProps = (state: State) => {
    const { items } = state.items;
    return { items };
}

export const ItemDetailWrapper = connect(mapStateToProps, {})(ItemDetail);