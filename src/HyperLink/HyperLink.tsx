import React from 'react';
import './HyperLink.scss';

export interface HyperLinkProps{
    href: string;
    title: string;
}

export const HyperLink: React.FC<HyperLinkProps> = (props)=>{
    return (<a
        className="App-link"
        href={props.href}
        target="_blank"
        rel="noopener noreferrer"
      >
        {props.title}
      </a>);
};