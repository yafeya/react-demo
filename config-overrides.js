module.exports = {
    webpack: function (config, env) {
        // configure to dist
        if (env === 'production') {
            const path = require('path');
            const paths = require('react-scripts/config/paths');
            paths.appBuild = path.join(path.dirname(paths.appBuild), 'dist');
            config.output.path = path.join(path.dirname(config.output.path), 'dist');

            const rewireUglifyjs = require('react-app-rewire-uglifyjs');
            config = rewireUglifyjs(config);

            const rewireCompressionPlugin = require('react-app-rewire-compression-plugin');
            config = rewireCompressionPlugin(config, env, {
                test: /\.js$|\.css$|\.html$/,
                cache: true,
                threshold: 10240,
                minRatio: 0.8
            });
        }
        return config;
    },
    // jest: function (config) {
    //   return config;
    // },
    devServer: function (configFunction) {
        return function (proxy, allowedHost) {
            const config = configFunction(proxy, allowedHost);
            config.disableHostCheck = true;
            config.historyApiFallback = true;
            return config;
        };
    },
    // paths: function (paths, env) {
    //   return paths;
    // },
}