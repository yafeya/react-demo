import { RouteComponentProps, Link } from "react-router-dom";
import logo from '../logo.svg';
import { Image } from "../Image/Image";
import { Paragraph } from "../Paragraph/Paragraph";
import { HyperLink } from "../HyperLink/HyperLink";

interface HomeProps extends RouteComponentProps {
}

async function dynamicLoadingTest() {
    let dynamicLoadingModule = await import('../DynamicLoading/dynamicLoading');
    let result = dynamicLoadingModule.default(1, 2);
    alert(`The result = ${result}`);
}

export const Home: React.FC<HomeProps> = (props) => {
    return (
        <div className="App">
            <header className="App-header">
                <Image src={logo} alt="logo"></Image>
                <Paragraph part1="Edit" code="src/App.tsx" part2="and save to reload."></Paragraph>
                <HyperLink href="https://reactjs.org" title="Learn React"></HyperLink>
                <Link to="/redux">Redux Demo</Link>
                <Link to="/items">Items List</Link>
                <Link to="/i18n">i18n Demo</Link>
                <button className="btn btn-primary" onClick={dynamicLoadingTest}>Dynamic Loading Test</button>
                <button className="btn btn-primary" onClick={props.history.goBack}>Back</button>
            </header>
        </div>
    );
}