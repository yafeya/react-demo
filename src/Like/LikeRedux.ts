export interface LikeState {
    count: number;
}

export const FETCH_LIKE_COUNT = 'FETCH_LIKE_COUNT';

export interface LikeAction {
    type: string;
    payload: number;
}

export function onLike(count: number): LikeAction {
    return {
        type: FETCH_LIKE_COUNT,
        payload: count
    };
}

const initState: LikeState = {
    count: 0
};

export function likeReducer(state = initState, action: any): LikeState {
    let result = initState;
    switch (action.type) {
        case FETCH_LIKE_COUNT:
            result = {
                ...state,
                count: action.payload
            };
            break;
        default:
            result = state;
            break;
    }
    return result;
}