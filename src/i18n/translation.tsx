import React from 'react';
import { useTranslation, Trans, Translation } from 'react-i18next';
import { RouteComponentProps } from 'react-router-dom';
import './translation.scss';
import logo from '../logo.svg';
import { Image } from '../Image/Image';
import { Paragraph } from '../Paragraph/Paragraph';
import { HyperLink } from '../HyperLink/HyperLink';

interface TranslationProps extends RouteComponentProps {
}

const TranslationWrapper: React.FC<TranslationProps> = (props) => {
    let { t, i18n } = useTranslation();

    function onclick() {
        let targetLanauge = i18n.language === 'en' ? 'zh' : 'en';
        i18n.changeLanguage(targetLanauge);
    }

    return (
        <div className="App">
            <header className="App-header">
                <Image src={logo} alt="logo"></Image>
                <Paragraph part1="Edit" code="src/App.tsx" part2="and save to reload."></Paragraph>
                <HyperLink href="https://reactjs.org" title="Learn React"></HyperLink>
                <div className='translationWrapper'>
                    <div className='translationGroup'>
                        <label>{t('firstWay')}</label>
                        <label>{t('testing')}</label>
                    </div>
                    <div className='translationGroup'>
                        <label>
                            <Trans>secondWay</Trans>
                        </label>
                        <label>
                            <Trans>testing</Trans>
                        </label>
                    </div>
                    <div className='translationGroup'>
                        <label>
                            <Translation>{t => <div>{t('thirdWay')}</div>}</Translation>
                        </label>
                        <label>
                            <Translation>{t => <div>{t('testing')}</div>}</Translation>
                        </label>
                    </div>
                    <div className='translationGroup'>
                        <button className='btn btn-primary' onClick={() => onclick()}>{t('switchLanguage')}</button>
                    </div>
                    <div className='translationGroup'>
                        <button className="btn btn-primary" onClick={props.history.goBack}>{t('back')}</button>
                    </div>
                </div >
            </header>
        </div>
    );
};

export default TranslationWrapper;