import { ClockState } from "../Clock/ClockRedux";
import { ItemListState } from "../ItemList/ItemRedux";
import { LikeState } from "../Like/LikeRedux";

export interface State {
    clock: ClockState;
    like: LikeState;
    items: ItemListState;
}